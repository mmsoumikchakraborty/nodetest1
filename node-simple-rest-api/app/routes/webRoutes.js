// loading verifyTokenMiddleware middleware
var verifyTokenMiddleware = require("../middleware/verifyTokenMiddleware");

var express = require('express');
var router = express.Router();


var webController = require("../controller/web/index");

// GET user list
router.get('/users', webController.users.getUserList);
// Post add user 
router.post('/user/add', webController.users.postSaveUser);
// Post update user 
router.post('/user/update', verifyTokenMiddleware.checkAccessToken, webController.users.postUpdateUser);
// Post login 
router.post('/login', webController.users.postLogin);

module.exports = router;