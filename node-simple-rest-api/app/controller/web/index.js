function getWebControllers(){

	var controllerObj = Object.create({});
	controllerObj.users = require("./userController.js");

	return controllerObj;
}

module.exports = getWebControllers();