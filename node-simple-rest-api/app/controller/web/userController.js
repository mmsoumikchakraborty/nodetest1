
const dbConObj = require("../../db/mysql_con").dbConnection;
const User = require("../../model/userModel").User;
const Auth = require("../../auth/auth");
module.exports = {

	getUserListMySqlCustom : (req, res, next) => {
			try{ 

				var userDataResp = {};
				dbConObj.getConnection(function(err, connection) {				
					  try{
					  		//var ttt= obj.name;
					  		if (err){
					  			console.log("****DB not connected error*****");
					  			res.status('500').json({success:0,message:"DB not connected"}); // not connected!
					  		}					  		
					 
							  // Use the connection
							connection.query('SELECT * FROM users', function (error, results, fields) {
								  		userDataResp = results; 					
										res.status('200').json(userDataResp);

									    // When done with the connection, release it.
									    connection.release();									 
									    
									    // Handle error after the release.
										if (error)
											res.status('500').json({success:0,message:error.code+' | '+error.sqlMessage});					 
									    // Don't use the connection here, it has been returned to the pool.
  	
							  });

					}
					catch(error){			
							res.status('500').json({success:0,message:''});
					}

				});				
			}
			catch(error){				
				res.status('500').json({success:0,message:''});
			}
		
	},
	getUserList : (req, res, next) => {
			try{ 

				var userDataResp = {};
				User.where('id', '>', 0).fetchAll().then(function(user) {
				  userDataResp = user.toJSON();
				  res.status('200').json({success:1,data:userDataResp,message:''});

				}).catch(function(err) {
					  console.error(err);
					}
				);		

			}
			catch(error){				
				res.status('500').json({success:0,message:''});
			}		
	},
	postSaveUser : (req, res, next) => {
		let postData = req.body;
		User.where({email: postData.email}).fetch().then(function(model) {
			let userDataResp = '';
			if(model!==null) userDataResp = model.toJSON();		

			// insert record
			if(model===null){				
				User.forge({name: postData.name, email: postData.email, role: ''}).save().then(function(model) {
					res.status('200').json({success:1,data:model.toJSON(),message:'record inserted'});

				}).catch(function(err) {
					  console.error(err);
					  res.status('500').json({success:0,data:'',message:''});
					});
			}
			else{
				// update or return record already exist
				res.status('402').json({success:0,data:'',message:'record already exist'});
			}


			

		}).catch(function(err) {
					  console.error(err);
					  res.status('500').json({success:0,data:'',message:''});
					}
		);	
	},
	postUpdateUser : (req, res, next) => {
		let postData = req.body;
		User.where({id: postData.id}).fetch().then(function(model) {
			let userDataResp = '';
			// updatevar jwt = require('jsonwebtoken');var jwt = require('jsonwebtoken'); record
			if(model!==null){				
				model.save({name: postData.name, email: postData.email, role: ''}).then(function(model) {
					res.status('200').json({success:1,data:model.toJSON(),message:'record updated'});

				}).catch(function(err) {
					  //console.error(err);
					  res.status('500').json({success:0,data:'',message:''});
					});
			}
			else{
				// update or return record already exist
				res.status('402').json({success:0,data:'',message:'record does not exist'});
			}


			

		}).catch(function(err) {
					  console.error(err);
					  res.status('500').json({success:0,data:'',message:''});
					}
		);	
	},
	postLogin : (req, res, next) => {
			try{ 

				let postData = req.body;
				var userDataResp = {};
				User.where('id', '=',postData.id).fetch().then(function(user) {
				  userDataResp = user.toJSON();
				  let accTokenObj = Auth.getJwtAccessToken(user.toJSON());
				  if(accTokenObj!==null && accTokenObj.success==1){
				  		tokenData = accTokenObj.data.token;							
						res.status('200').json({success:1,data:{user:userDataResp,token:tokenData},message:''});
				  }else{
				  		 res.status('500').json({success:0,message:''});
				  }				  

				}).catch(function(err) {
					  console.error(err);
					}
				);		

			}
			catch(error){				
				res.status('500').json({success:0,message:''});
			}		
	}
}