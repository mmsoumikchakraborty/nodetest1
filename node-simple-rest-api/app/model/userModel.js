const bkShelf = require("../db/bookshelf_orm").bookshelf;
var userModel = bkShelf.Model.extend({
  tableName: 'users'
});

module.exports ={
	User: userModel
}