const dbConf = require("../config/app_config");

var knex = require('knex')({
  client: 'mysql',
  connection: {
    host     : dbConf.mySql.host,
    user     : dbConf.mySql.db_user_name,
    password : dbConf.mySql.db_password,
    database : dbConf.mySql.db_name,
    charset  : dbConf.mySql.db_charset
  }
});

var bookshelf = require('bookshelf')(knex);

module.exports ={
	bookshelf: bookshelf
}