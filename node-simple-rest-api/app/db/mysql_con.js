var mysql = require('mysql');
const dbConf = require("../config/app_config");


var mySqlCon = mysql.createPool({
	host: dbConf.mySql.host,
	user: dbConf.mySql.db_user_name,
	password: dbConf.mySql.db_password,
	database: dbConf.mySql.db_name
});



mySqlCon.on('release', function (connection) {
  console.log('Connection %d released', connection.threadId);
});

mySqlCon.on('enqueue', function () {
  console.log('Waiting for available connection slot');
});


module.exports ={
	dbConnection: mySqlCon
}