module.exports = ((env) => {
	var appConfigObj = {};
 	switch(env){ 		
 		case 'local':
	 		appConfigObj = require("./env/local_config");
	 		break;
 		case 'development':
	 		appConfigObj = require("./env/development_config");
	 		break;
 		case 'production':
	 		appConfigObj = require("./env/production_config");
	 		break;
 		default:
	 		console.log("NODE_ENV not set");
	 		process.exit(1);
 	}
 	return appConfigObj;
})(process.env.NODE_ENV||'development');