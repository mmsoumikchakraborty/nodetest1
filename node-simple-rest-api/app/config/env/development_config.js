module.exports = {
	mySql: {
			 'host': 'localhost',
			 'db_name': 'testdb1',
			 'db_user_name': 'root',
			 'db_password': 'matrix',
			 'db_port': '',
			 'db_charset': 'utf8'
	},
	server: (process.env.NODE_SERVER_IP_ADDR||'192.168.1.80'),
	port: (process.env.PORT || '5009'),
	tokenExpiresIn: '60s',
	tokenSecretKey: 'ripedmango'

}