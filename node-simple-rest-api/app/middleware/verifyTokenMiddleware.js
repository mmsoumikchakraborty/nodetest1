const Auth = require("../auth/auth");
module.exports = {
	'checkAccessToken': (req, res, next)=> {
		try{		
			const bearerHeader = req.headers['authorization'];
			if(typeof bearerHeader !== 'undefined' ){
				const bearer = bearerHeader.split(' ');
				const bearerToken = bearer[1];

				let chkVeriStatus = Auth.verifyToken(bearerToken);
				if(chkVeriStatus.success==0){
						res.send({'success':0,'message':chkVeriStatus.message});
				}else{
					req.token = bearerToken;
					next();
				}					

			}else{
				res.send({'success':0,'message':'Token not provided'});
			}
		}catch(err){		
			res.send({'success':0,'message':'Unable to checkAccessToken'});
		}

	}
}