var jwt = require('jsonwebtoken');
const appConf = require("../config/app_config");
module.exports = {
	getJwtAccessToken: (userJsonObj)=>{
		let respData = {'success':0,'message':'something went wrong'};
		if(userJsonObj==null) return respData;
		let tokenObj = jwt.sign(userJsonObj, appConf.tokenSecretKey, {
	      expiresIn: appConf.tokenExpiresIn}   ); //console.log("-tokenObj=>",tokenObj);
		  respData = {'success':1,'message':'', 'data': {'token':tokenObj}};
		  return respData;
		},
	verifyToken: (token) => {

		try{
			 var chkRes = jwt.verify(token, appConf.tokenSecretKey);
			 //console.log(chkRes);
			 if(chkRes){
			 	return {success:1,message:'ok'};
			 }else{
			 	return {success:0,message:'something went wrong while verifying token'};
			 }
			 
		}catch(err){
			if(err.message == 'jwt expired'){
				return {success:0,message:'token expired'};
			}
			return {success:0,message:'something went wrong while verifying token'};
		}
		
		

	}	

}