const appConfig = require("./app/config/app_config"); // configuration file inclusion

var http = require("http");
var express = require('express');
var bodyParser = require('body-parser');
// loading routes
var webApiRouter = require("./app/routes/webRoutes");



const port = appConfig.port;
const serverIp = appConfig.server;

var app = express();
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));

var server = app.listen(port, serverIp, function () {
  var host = server.address().address
  var port = server.address().port  
  console.log("listening at http://%s:%s", host, port)
});

// include routes
app.use('/web-api', webApiRouter);

// It should be defined last
app.use(function (err, req, res, next) {
  console.error(err.stack)
  res.status(500).send({'error':1,'message':'Something went wrong'})
});